//
//  Data.swift
//  Weather
//
//  Created by Admin on 8/16/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

//MARK: - Types

enum StationKeys: String {
    case name = "name"
    case url = "url"
}

let stationsList = [[StationKeys.name.rawValue : "Aberporth",
                          StationKeys.url.rawValue : "http://www.metoffice.gov.uk/pub/data/weather/uk/climate/stationdata/aberporthdata.txt"],
                         [StationKeys.name.rawValue : "Armagh",
                          StationKeys.url.rawValue : "http://www.metoffice.gov.uk/pub/data/weather/uk/climate/stationdata/armaghdata.txt"],
                         [StationKeys.name.rawValue : "Ballypatrick Forest",
                          StationKeys.url.rawValue : "http://www.metoffice.gov.uk/pub/data/weather/uk/climate/stationdata/ballypatrickdata.txt"],
                         [StationKeys.name.rawValue : "Bradford",
                          StationKeys.url.rawValue : "http://www.metoffice.gov.uk/pub/data/weather/uk/climate/stationdata/bradforddata.txt"],
                         [StationKeys.name.rawValue : "Camborne",
                          StationKeys.url.rawValue : "http://www.metoffice.gov.uk/pub/data/weather/uk/climate/stationdata/cambornedata.txt"],
                         [StationKeys.name.rawValue : "Cambridge NIAB",
                          StationKeys.url.rawValue : "http://www.metoffice.gov.uk/pub/data/weather/uk/climate/stationdata/cambridgedata.txt"],
                         [StationKeys.name.rawValue : "Cardiff Bute Park",
                          StationKeys.url.rawValue : "http://www.metoffice.gov.uk/pub/data/weather/uk/climate/stationdata/cardiffdata.txt"],
                         [StationKeys.name.rawValue : "Chivenor",
                          StationKeys.url.rawValue : "http://www.metoffice.gov.uk/pub/data/weather/uk/climate/stationdata/chivenordata.txt"]]

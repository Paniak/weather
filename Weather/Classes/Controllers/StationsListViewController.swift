//
//  StationsListViewController.swift
//  Weather
//
//  Created by Admin on 8/16/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class StationsListViewController: UIViewController {

    // MARK: - Types 
    
    struct StoryboardIds {
        
        static let kStationTableViewCellReuseIdentifier = "StationTableViewCell"
        static let kSegueToStationVC = "segueToStationVC"
    }
    
    // MARK: - Properties
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == StoryboardIds.kSegueToStationVC {
            
            let stationVC: StationViewController = (segue.destination as? StationViewController)!
            
            if let stationUrl = sender as? String {
                stationVC.stationUrl = stationUrl
            }
        }
    }
}

// MARK: - UITableViewDataSource

extension StationsListViewController: UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  stationsList.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: StoryboardIds.kStationTableViewCellReuseIdentifier, for: indexPath) as! StationTableViewCell
        
        let station = stationsList[indexPath.row]
        cell.name.text = station[StationKeys.name.rawValue]
        
        return cell
    }
}

// MARK: - UITableViewDelegate

extension StationsListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        let station = stationsList[indexPath.row]
        performSegue(withIdentifier: StoryboardIds.kSegueToStationVC, sender: station[StationKeys.url.rawValue])
    }
}


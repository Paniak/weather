//
//  StationViewController.swift
//  Weather
//
//  Created by Admin on 8/16/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class StationViewController: UIViewController {
    
    // MARK: - Types
    
    struct StoryboardIds {
        
        static let kWeatherTableViewCellReuseIdentifier = "WeatherTableViewCell"
    }
    
    // MARK: - Properties
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var stationUrl = ""
    var station = Station()
    
    // MARK: - Controller lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupProperties()
    }
    
    // MARK: - Private method
    
    private func setupProperties () {
        station = Station(txt: stationUrl)
        tableView.reloadData()
        
        navigationItem.title = station.name
        infoLabel.text = station.info
    }
}

// MARK: - UITableViewDataSource

extension StationViewController: UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  station.weathers.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: StoryboardIds.kWeatherTableViewCellReuseIdentifier, for: indexPath) as! WeatherTableViewCell
        
        let weather = station.weathers[indexPath.row]
        
        cell.yyyyLabel.text = weather.yyyy
        cell.mmLabel.text = weather.mm
        cell.tMaxLabel.text = weather.tmax
        cell.tMinLabel.text = weather.tmin
        cell.afLabel.text = weather.af
        cell.rainLabel.text = weather.rain
        cell.sunLabel.text = weather.sun
        
        return cell
    }
}


//
//  StationTableViewCell.swift
//  Weather
//
//  Created by Admin on 8/16/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class StationTableViewCell: UITableViewCell {

    // MARK: - Properties
    
    @IBOutlet weak var name: UILabel!
}


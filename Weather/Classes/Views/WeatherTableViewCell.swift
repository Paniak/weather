//
//  WeatherTableViewCell.swift
//  Weather
//
//  Created by Admin on 8/16/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class WeatherTableViewCell: UITableViewCell {

    // MARK: - Properties
    
    @IBOutlet weak var yyyyLabel: UILabel!
    @IBOutlet weak var mmLabel: UILabel!
    @IBOutlet weak var tMaxLabel: UILabel!
    @IBOutlet weak var tMinLabel: UILabel!
    @IBOutlet weak var afLabel: UILabel!
    @IBOutlet weak var rainLabel: UILabel!
    @IBOutlet weak var sunLabel: UILabel!
}

//
//  Weather.swift
//  Weather
//
//  Created by Admin on 8/16/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class Weather: NSObject {

    // MARK: - Properties
    
    var yyyy = ""
    var mm = ""
    var tmax = ""
    var tmin = ""
    var af = ""
    var rain = ""
    var sun = ""
    
    // MARK: - Init methods
    
    init(strings: [String]) {
        
        yyyy = strings[0]
        mm = strings[1]
        tmax = strings[2]
        tmin = strings[3]
        af = strings[4]
        rain = strings[5]
        sun = strings[6]
    }
}

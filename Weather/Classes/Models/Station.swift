//
//  Station.swift
//  Weather
//
//  Created by Admin on 8/16/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class Station: NSObject {
    
    // MARK: - Properties
    
    var name = ""
    var info = ""
    var weathers = [Weather]()
    
    
    // MARK: - Init methods
   
    override init() {
    }
    
    init(txt urlString: String) {
        
        var contentsOfUrl = ""
        
        // Convert txt to String
        if let url = URL(string: urlString) {
            do {
                let contents = try String(contentsOf: url)
                contentsOfUrl = contents
            } catch {
                print("contents could not be loaded")
            }
        } else {
            print("the URL was wrong!")
        }
        
        // Create array with weather strings
        var weathersArray = [String]()
        weathersArray = contentsOfUrl.components(separatedBy: "\n")
        
        var stationMainInfoArray = [String]()
        for string in weathersArray[1..<5]{
            stationMainInfoArray.append(string)
        }
        
        // Joining strings from array in one string:
        info = stationMainInfoArray.joined(separator: " ")
        
        name = weathersArray.first!
        
        // Parsing weather data:
        var weatherStringsArray = [String]()
       
        var i = 0
        for weatherString in weathersArray {
            i += 1
            // Need to start with 8 lines
            if i > 7 && i < weathersArray.count {
                weatherStringsArray = weatherString.components(separatedBy: "  ")
              
                let charToDelete = [""]
                weatherStringsArray = weatherStringsArray.filter{ !charToDelete.contains($0)}

                let weather = Weather(strings: weatherStringsArray)
                weathers.append(weather)
            }
        }
    }
}
